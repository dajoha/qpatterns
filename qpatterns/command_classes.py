
import re

from .block import *


##
# Define a command which will search for a block.
class BlockCommandDef:
    def parse_func(self, qs):
        qs.debug(3, 'Block command <{}>'.format(self.open_char))

        if qs.state.start_from_end:
            blocks = []

        # Look for `count` blocks:
        i = 0
        while qs.state.start_from_end or i != qs.state.count:
            # Look for one of the chars defined for this command:
            found_block = qs.text.search_after(self.bd_start_chars)
            start = qs.text.index

            if found_block != -1:
                qs.debug(3, "Found block : {}".format(found_block))
                # Definition of the found block:
                block_def = self.block_defs[found_block]
                # If we don't find the block ending char, it doesn't matter, the search will
                # naturally reach the end of the string:
                qs.text.search_for(block_def.end_char, must_have_same_level = True)
            end = qs.text.index

            # If we have to perform a reverse search, store each found block:
            if qs.state.start_from_end and found_block != -1:
                qs.debug(3, '  Sauve bloc: {}->{}'.format(start, end))
                blocks.append(Block(block_def, start, end))

            if found_block == -1:
                if qs.state.start_from_end:
                    if (-qs.state.count) > len(blocks):
                        qs.debug(2, 'Reverse search <{}> : index {} before the first block'.format(self.open_char, qs.state.count))
                        return False
                    block = blocks[qs.state.count] # count < 0
                    block_def = block.block_def
                    start, end = block.start, block.end
                    qs.text.index = end
                    break
                else:
                    return False
            qs.text.skip_last_look()
            i += 1

        qs.set_last_close_block(block_def, start, end)

        # If we don't want to skip a whole block then go on:
        if not qs.qpat.look_for(self.void_block()):
            qs.enter_last_close_block()
            qs.debug(3, 'Enter the block {}'.format(qs.text.block_stack[-1].block_def.start_char))

        return True


    ##
    # Return the concatenation of the block opening and closing string, to identify a block to skip:
    def void_block(self):
        return self.open_char + self.close_char


    ##
    # Constructor
    #
    # @param open_char   Char which indicates the search of a block start
    # @param close_char  Char which indicates the search of a block end
    # @param block_defs  List of block definitions (BlockDef) associated with this command
    def __init__(self, open_char, close_char, block_defs):
        self.open_char = open_char
        self.close_char = close_char
        self.block_defs = block_defs
        self.bd_start_chars = [ bd.start_char for bd in block_defs ]
        self.bd_end_chars = [ bd.end_char for bd in block_defs ]




##
# Define a command which will search for a delimitor.
class DelimCommandDef:
    def parse_func(self, qs):
        qs.debug(3, 'Delimitor command <{}>'.format(self.name))

        if qs.state.start_from_end:
            blocks = []

        i = 0
        while i != qs.state.count or qs.state.start_from_end:
            # Look for the next delimitor:
            start = qs.text.index
            found_block = qs.text.search_for(self.bd_start_chars, must_have_same_level = True)
            end = qs.text.index

            # If we have to perform a reverse search, store each found block, even if we didn't find
            # the next delimitor (in this case, this is the final block):
            if qs.state.start_from_end:
                qs.debug(3, '  Store block: {}->{}'.format(start, end))
                blocks.append(Block(None, start, end))

            if found_block != -1:
                # Skip the delimitor in order to continue the search:
                qs.text.skip_last_look()
            else:
                if qs.state.start_from_end:
                    if (-qs.state.count) > len(blocks):
                        qs.debug(2, 'Reverse search <{}> : index {} before the first block'.format(self.name, qs.state.count))
                        return False
                    block = blocks[qs.state.count] # count < 0
                    start, end = block.start, block.end
                    qs.text.index = end
                    break
                elif i != qs.state.count - 1:
                    qs.debug(2, 'Delimitor <{}> {} not found'.format(self.name, qs.state.count))
                    return False

            i += 1

        qs.debug(3, 'Last close block, start={}, end={}'.format(start, end))
        qs.set_last_close_block(None, start, end)

        # If the delimitor is not doubled, then we enter into the block:
        if not qs.qpat.look_for(self.name + '<'):
            qs.enter_last_close_block()

        return True


    ##
    # Constructor
    #
    # @param name        Name of the command
    # @param block_defs  List of block definitions (BlockDef) to search for
    def __init__(self, name, block_defs):
        self.name = name
        self.block_defs = block_defs
        self.bd_start_chars = [ bd.start_char for bd in block_defs ]




##
# Define a command which will search for an operator.
class OperCommandDef(DelimCommandDef):
    pass




##
# Define commands which are shortcuts for regex searches.
class RegexShortcutDef:

    ##
    # Constructor
    #
    # @param name          Name of the command
    # @param regex_string  Regex
    def __init__(self, name, regex_string):
        self.name = name
        self.regex_string = regex_string
        self.regex = re.compile(regex_string)


