
from .smartstr import *



# A generic block definition for regex bocks:
regex_block_def = BlockDef(None, None)

# A generic block definition for end-of-line block:
eol_block_def = BlockDef(None, None)


# Regex for parsing counts in qpatterns:
count_regex = re.compile(r'-?\d+')

# Regex parsing spaces in qpatterns:
spaces_regex = re.compile(r'\s+')




##
# Represent the result of a search
class QMatch:

    ##
    # Constructor
    #
    # @param source   Source input text
    # @param match    The found string (=part of the input text)
    # @param start    Start position in the input
    # @param end      End position (exclusive) in the input
    def __init__(self, source, match, start, end):
        self.source  = source
        self.match   = match
        self.start   = start
        self.end     = end


    ##
    # Return the result as a dictionnary
    def to_dict(self):
        return {
            'source'  : self.source,
            'match'   : self.match,
            'start'   : self.start,
            'end'     : self.end,
        }


    ##
    # Return the found string
    def __repr__(self):
        return self.match


    def debug_disp():
        print("Matched: {}\nStart: {}  End: {}\nSource: '{}'".format(
                self.match, self.start, self.end, self.source))




#-------------------- class Star
class Star:
    def __init__(self, qpat, text):
        self.qpat = qpat.clone()
        self.text = text.clone()
        self.count = 1

    def __repr__(self):
        output = ''
        output += "  Star:\n"
        output += "   qpat: {}\n".format(self.qpat)
        output += "   text: {}\n".format(self.text)
        output += "   count: {}\n".format(self.count)
        return output



##
# Perform some searches from a given qpattern.
# Could be compared to `re.compile()`, but for qpatterns.
# However, for now it doesn't compile anything, that's why it's very slow compared to what it could be.
class QSearch:

    ##
    # Represent the internal state during the qpattern parse.
    class State:
        def __init__(self):
            # Is there a defined count for the next comand:
            self.count = None
            # Was the defined count a negative one:
            self.start_from_end = None
            # Is there a defined count:
            self.has_count = False

    ##
    # Set the last created closed block, just before in the pattern flow.
    #
    # @param block_def  Block definition
    # @param start      Position of the start of the block
    # @param end        Position of the end of the block
    def set_last_close_block(self, block_def, start, end):
        rb = self.text.restrict_block
        if rb:
            if start < rb.start: start = rb.start
            if start > rb.end:   start = rb.end
            if end < rb.start: end = rb.start
            if end > rb.end:   end = rb.end
        self.last_close_block = Block(block_def, start, end)


    ##
    # Reset the last created closed block to `None`.
    def reset_last_close_block(self):
        self.last_close_block = None


    ##
    # Enter the last created closed block (action performed by the '>' command)
    #
    # @param force  Don't generate error if there is no last closed block
    def enter_last_close_block(self, force=False):
        if self.last_close_block:
            self.debug(3, "Enter the last block")
            self.text.block_stack_push_block(self.last_close_block)
            self.text.restrict_inside_last_block()
            self.text.index = self.last_close_block.start
            self.reset_last_close_block()
        elif not force:
            self.debug(2, "Error: misplaced '>' sign")


    #-------------------- def push_new_star(self)
    def push_new_star(self):
        self.star_stack.append(Star(self.qpat, self.text))


    #-------------------- def inc_cur_star(self)
    def inc_cur_star(self):
        self.star_stack[-1].count += 1
        self.text = self.star_stack[-1].text.clone()
        self.qpat = self.star_stack[-1].qpat.clone()


    #-------------------- def debug_star(self)
    def debug_star(self):
        if debug < 2: return
        i = 0
        print("STARSTACK:")
        for star in self.star_stack:
            print(i, star)
            i += 1


    ##
    # Debug
    #
    # @param level  Level starting from which the message will be displayed
    # @param msg    Message to display
    def debug(self, level, msg=''):
        printdbg(level, "[{}] {}".format(self.qpat.index, msg))


    ##
    # Search the occurence(s) of a qpattern in a string.
    #
    # @param text  Text to search
    #
    # @return  List of `QMatch` objects
    def search(self, text):
        self.text = SmartStr(text)
        self.qpat.rewind()
        RESULTS = []
        is_starred_com = False

        debug_i = 2000

        while True:

            debug_i = debug_i - 1
            if debug_i == 0:
                fatal_error("Internal error: infinite loop")

            found_command = False
            text_found = False

            self.debug(3, "Start of loop, qpat index: {}, text index: {}".format(
                self.qpat.index, self.text.index))

            if self.qpat.skip_search(spaces_regex):
                self.debug(3, "Skip spaces")
                if self.qpat.end():
                    break

            self.reset_last_close_block()

            # Look for an optionnal count:
            match_count = self.qpat.look_for_regex(count_regex)
            if match_count:
                self.state.count = int(self.qpat.last_look)
                self.state.has_count = True
                self.qpat.skip_last_look()
            else:
                self.state.count = 1
                self.state.has_count = False

            # Do we perform a reverse search?
            self.state.start_from_end = self.state.count < 0
            if self.state.start_from_end:
                blocks = []

            # ********** STAR:

            self.debug_star()
            if self.qpat.look_for('*'):
                self.debug(3, 'STAR: found')
                self.qpat.skip_last_look()
                self.push_new_star()
                is_starred_com = True
            if is_starred_com:
                self.state.count = self.star_stack[-1].count
                self.state.has_count = True


            self.debug(3, "Count: {}".format(self.state.count))


            # BLOCK COMMANDS:
            if not found_command:
                for block_command in self.syn.block_commands:

                    # If we found a command block:
                    if self.qpat.look_for(block_command.open_char):

                        if block_command.parse_func(self):
                            text_found = True
                            #  return None

                        # Continue the read of the qpattern:
                        found_command = True
                        break # Leave command blocks


            # DELIMITOR COMMANDS:
            if not found_command:
                for delim_command in self.syn.delim_commands:
                    if self.qpat.look_for(delim_command.name):

                        if delim_command.parse_func(self):
                            text_found = True
                            #  return None

                        found_command = True
                        break # Leave delimitor commands


            # OPERATOR COMMANDS:

            # Look for a 'lhs' operator command:
            if not found_command and self.qpat.look_for(self.syn.lhs_oper_sign):
                if self.state.has_count:
                    fatal_error('Count is forbidden for "lhs" operator commands')
                com_start = self.qpat.last_look
                self.debug(3, 'com_start = {}'.format(com_start))
                for oper_command in self.syn.oper_commands:

                    if not self.qpat.look_for(self.syn.lhs_oper_sign+oper_command.name):
                        continue

                    self.debug(3, '"lhs" operator command <{}>'.format(com_start+oper_command.name))

                    # Look for the operator:
                    start = self.text.index
                    found_block = self.text.search_for(oper_command.bd_start_chars, must_have_same_level = True)

                    if found_block != -1:
                        text_found = True

                        self.debug(3, 'Last close block, start={}, end={}'.format(start, self.text.index))
                        block_def = oper_command.block_defs[found_block]
                        self.set_last_close_block(block_def, start, self.text.index)

                        self.enter_last_close_block()

                        found_command = True
                        break
                    else:
                        self.debug(2, 'Operator {} not found'.format(oper_command.bd_start_chars[0]))

            # Look for a 'rhs' operator command:
            if not found_command and self.qpat.look_for(self.syn.rhs_oper_sign):
                com_start = self.qpat.last_look
                for oper_command in self.syn.oper_commands:

                    if not self.qpat.look_for(self.syn.rhs_oper_sign+oper_command.name):
                        continue

                    if self.state.has_count:
                        fatal_error('Count is forbidden for "rhs" oprator commands')

                    self.debug(3, '"rhs" operator command <{}>'.format(com_start+oper_command.name))

                    # Look for the operator:
                    found_block = self.text.search_for(oper_command.bd_start_chars, must_have_same_level = True)

                    if found_block != -1:
                        text_found = True

                        self.text.skip_last_look()

                        found_command = True
                        break
                    else:
                        self.debug(2, 'Operator {} not found'.format(oper_command.bd_start_chars[0]))


            # REGEX COMMAND:

            # `looked_command` is used to handle in end of loop the special case of '/', when parsing
            # the command (indeed, `last_look` is empty at the command's end, which is not the case
            # for all other commands):
            looked_command = None

            if not found_command and self.qpat.look_for('/'):
                looked_command = True

                # Get the regex:
                regex_string = self.qpat.get_inner_string()
                self.debug(3, "Search command </>, regex: /{}/".format(regex_string))

                regex = re.compile(regex_string)

                i = 0
                while self.state.start_from_end or i != self.state.count:
                    # Search into the text:
                    found = self.text.search_for(regex)
                    text_found = False
                    if found:
                        text_found = True
                        self.text.skip_last_look()
                        match = self.text.last_match
                        start, end = match.start(0), match.end(0)
                        if self.state.start_from_end:
                            blocks.append(Block(regex_block_def, match.start(0), match.end(0)))
                    else:
                        if self.state.start_from_end:
                            if (-self.state.count) > len(blocks):
                                self.debug(2, 'Reverse search <{}> : index {} before the first block'.format('/', self.state.count))
                            else:
                                text_found = True
                                block = blocks[self.state.count] # count < 0
                                start, end = block.start, block.end
                                self.text.index = end
                        else:
                            self.debug(2, "Pattern {} not found".format(self.state.count))
                        break
                    i += 1

                if text_found:
                    self.set_last_close_block(regex_block_def, start, end)

                found_command = True


            # REGEX SHORTCUTS:

            if not found_command:
                for shortcut_command in self.syn.regex_shortcut_commands:
                    if self.qpat.look_for(shortcut_command.name):

                        i = 0
                        while self.state.start_from_end or i != self.state.count:
                            # Search into the text:
                            found = self.text.search_for(shortcut_command.regex)
                            text_found = False
                            if found:
                                text_found = True
                                self.text.skip_last_look()
                                match = self.text.last_match
                                start, end = match.start(0), match.end(0)
                                if self.state.start_from_end:
                                    blocks.append(Block(regex_block_def, match.start(0), match.end(0)))
                            else:
                                if self.state.start_from_end:
                                    if (-self.state.count) > len(blocks):
                                        self.debug(2, 'Reverse search <{}> : index {} before the first block'
                                                .format('/', self.state.count))
                                    else:
                                        text_found = True
                                        block = blocks[self.state.count] # count < 0
                                        start, end = block.start, block.end
                                        self.text.index = end
                                else:
                                    self.debug(2, "Pattern {} not found".format(self.state.count))
                                break
                            i += 1

                        if text_found:
                            self.set_last_close_block(regex_block_def, start, end)

                        found_command = True
                        break # Leave regex shortcut commands

            # END-OF-LINE COMMAND:
            if not found_command:
                if self.qpat.look_for('$'):
                    if self.state.has_count:
                        fatal_error('Count is forbidden for the command "$"')

                    self.debug(3, 'End-of-line command <$>')

                    end = self.text.restrict_block and self.text.restrict_block.end or len(self.text.string)
                    self.set_last_close_block(eol_block_def, self.text.index, end)
                    self.enter_last_close_block()

                    found_command = True


            # END OF LOOP:

            self.debug(3, "End of loop (last look : {})".format(self.qpat.last_look))

            if not (self.qpat.last_look or looked_command):
                fatal_error('Erreur: Commande non reconnue au début de: {}, position {}'.format(self.qpat.string[self.qpat.index:], self.qpat.index))
                fatal_error('Error: Unknown command at this point: {}, position {}'.format(self.qpat.string[self.qpat.index:], self.qpat.index))

            # Skip the sub-command parsed during the loop iteration:
            self.qpat.skip_last_look()

            # Handle the command to enter the last closed block:
            if self.qpat.look_for('>'):
                self.debug(3, 'Commande >')
                self.enter_last_close_block()
                self.qpat.skip_last_look()

            #  print("star_stack? ", not not self.star_stack)
            #  print("text.end() ?", not not self.text.end())
            #  print("qpat.end() ?", not not self.qpat.end())
            #  print("is_starred_com: ", is_starred_com)

            if is_starred_com:
                self.debug(2, '  is_starred_com = True')
                if not text_found:
                    self.debug(2, '    TEXT END (pop star_stack)')
                    self.star_stack.pop()
                    if self.star_stack:
                        self.debug(2, '      Still star_stack')
                        self.inc_cur_star()
                        is_starred_com = True
                        continue
                    else:
                        self.debug(2, '       No star_stack: BREAK')
                        break


            if self.qpat.end():
                self.debug(2, '  QPAT END')
                self.enter_last_close_block(force = True) # force = don't generate error
                result, start, end = self.text.get_text()
                if result:
                    RESULTS.append(QMatch(self.text.string, result, start, end))
                    self.debug(2, 'NEW RESULT: {}'.format(result))
                if self.star_stack:
                    self.debug(2, '    star_stack -> inc_cur_star()')
                    self.inc_cur_star()
                    is_starred_com = True
                else:
                    self.debug(2, '    No star_stack -> BREAK')
                    break
            else:
                self.debug(2, '  qpat : not finished')
                is_starred_com = False

            self.debug(3)

        # Enter automatically into the last block, if not already done:
        self.enter_last_close_block(force = True) # force = don't generate error

        result, start, end = self.text.get_text()

        printdbg(3, 'Result:')
        printdbg(3, result)
        printdbg(3, 'RESULTS:')
        if debug > 0:
            for r in RESULTS:
                print(r)

        return RESULTS


    ##
    # Constructor
    #
    # @param qpat  Qpattern to use
    def __init__(self, qpat, syntax=generic_syn):
        self.qpat = SmartStr(
            qpat,
            # Allow to escape '/' chars inside regex commands:
            [ BlockDef('/', '/', is_string = True) ]
        )
        self.syn = syntax
        self.reset_last_close_block()
        self.state = self.State()
        self.star_stack = []




##
# Perform a simple search: return the list of found strings
#
# @param text     Input text
# @param pattern  Qpattern
#
# @return  List of found strings
def searchstr(text, pattern):
    qs = QSearch(pattern, generic_syn)
    return [ m.match for m in qs.search(text) ]




##
# Perform a simple search: return a list of dictionnaries representing the found elements.
#
# @param text     Input text
# @param pattern  Qpattern
#
# @return  List of dictionnaries
def searchdict(text, pattern):
    qs = QSearch(pattern, generic_syn)
    return [ m.to_dict() for m in qs.search(text) ]




##
# Perform a simple search: return a list of `QMatch` objects.
#
# @param text     Input text
# @param pattern  Qpattern
#
# @return  List of `QMatch` objects
def search(text, pattern):
    return QSearch(pattern, generic_syn).search(text)

