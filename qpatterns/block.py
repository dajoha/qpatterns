

##
# Represent the definition of a qpatterns block.
class BlockDef:

    ##
    # Constructor
    #
    # @param start_char  Character or string which identifies the start of the block
    # @param end_char    Character or string which identifies the end of the block
    # @param is_string   Define if this block should be considered as a string
    def __init__(self, start_char, end_char, is_string = False):
        self.start_char = start_char
        self.end_char = end_char
        self.is_string = is_string




##
# Represent an instance of qpatterns block.
class Block:

    ##
    # Constructeur
    #
    # @param block_def  Block definition
    # @param start      Offset of the block start in the input string
    # @param end        Offset of the block end in the input string, or `None` if unknown
    def __init__(self, block_def, start, end = None):
        self.block_def = block_def
        self.start = start
        self.end = end

    def __repr__(self):
        if self.block_def:
            return "(Block <{}>, start={}, end={})".format(self.block_def.start_char, self.start, self.end)
        else:
            return "(Block <no blockdef>, start={}, end={})".format(self.start, self.end)


