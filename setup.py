
from setuptools import setup

setup(
    name='qpatterns',
    version='0.01',
    description='A library to parse strings in an easy, semantic way',
    url='',
    author='Johann DAVID',
    author_email='',
    license='MIT',
    packages=['qpatterns'],
    zip_safe=False
)
