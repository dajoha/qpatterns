

class QException(Exception):
    pass


debug = 0


##
# Display a message only if the debug level is enough high.
#
# @param level    Level starting from which the message will be displayed
# @param message  Message to display
def printdbg(level, message='', end='\n'):
    if debug >= level:
        print(message, end=end)


##
# Handle fatal error.
#
# @param message  Error message to display
def fatal_error(message):
    #  printdbg(2, message)
    raise QException(message)


##
# Return one line in a text file.
#
# @param  filename
# @param  linenr
#
# @return  String
def read_one_line(filename, linenr):
    i = 1
    with open(filename, 'r') as inline:
        for line in inline:
            if i == linenr: return line[:-1]
            i = i + 1


